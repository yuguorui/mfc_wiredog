// ChildView.h : CChildView 类的接口
//

#pragma once
#include "StaticTransparent.h"
#include "PacketCaptor.h"

#define WM_NEW_PACKET (WM_USER+0)
#define WM_WORKER_THREAD_STOP (WM_USER+1)

// CChildView 窗口

class CChildView : public CWnd
{
    // 构造
public:
    CChildView();

    // 特性
    CFont font;
    CStaticTransparent m_staticText;
    CButton m_buttonStart;
    CButton m_buttonStop;
    CString m_strAdapter;
    CListCtrl m_listctrlPackets;
    CTreeCtrl m_treeCtrl;

    pcap_if_t *allDevices;          // released when we try to choose the adapter.
    int m_deviceIndex;
    int m_packetCount;
    CArray<PacketInfo*> allPackets;
    WorkerThreadInfo* threadInfo;   // the memory of threadinfo is released at worker thread.

public:
    // 操作
    
    void addNewPacket(int no, CString time, CString source, CString dest, CString protocol, CString transmit = NULL);
    void showInTreeView(int packetIndex);
public:

    // 重写
protected:
    virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

    // 实现
public:
    virtual ~CChildView();

    // 生成的消息映射函数
protected:
    afx_msg void OnPaint();
    DECLARE_MESSAGE_MAP()
public:
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnChooseadapter();
    void clear();
    void OnStartButtonClicked();
    void OnStopButtonClicked();
    void OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnSize(UINT nType, int cx, int cy);
protected:
    afx_msg LRESULT OnNewPacket(WPARAM wParam, LPARAM lParam);
    afx_msg LRESULT OnWorkerThreadStop(WPARAM wParam, LPARAM lParam);
};

