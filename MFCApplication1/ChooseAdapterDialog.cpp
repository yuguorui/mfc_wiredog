﻿// ChooseAdapterDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "MFCApplication1.h"
#include "ChooseAdapterDialog.h"
#include "afxdialogex.h"


// ChooseAdapterDialog 对话框

IMPLEMENT_DYNAMIC(ChooseAdapterDialog, CDialog)

ChooseAdapterDialog::ChooseAdapterDialog(CWnd* pParent /*=NULL*/)
    : CDialog(IDD_ChooseDialog, pParent)
{
}

BOOL ChooseAdapterDialog::OnInitDialog()
{
    CDialog::OnInitDialog();

    char errBuf[PCAP_ERRBUF_SIZE];
    if (allDevices != NULL)
    {
        pcap_freealldevs(allDevices);
    }
    pcap_findalldevs_ex(PCAP_SRC_IF_STRING, NULL, &allDevices, errBuf);
    for (tmp = allDevices; tmp != NULL; tmp = tmp->next)
    {
        CString name(tmp->description);
        m_AdapterList.AddString(name);
    }


    return 0;
}

void ChooseAdapterDialog::OnOK()
{

    if ((m_deviceIndex = m_AdapterList.GetCurSel()) != LB_ERR)
    {
        int i = 0;
        for (tmp = allDevices; i < m_deviceIndex; tmp = tmp->next, i++);
        m_strAdapterName = tmp->name;
    }
    CDialog::OnOK();
}

ChooseAdapterDialog::~ChooseAdapterDialog()
{
}

void ChooseAdapterDialog::DoDataExchange(CDataExchange* pDX)
{
    DDX_Control(pDX, IDC_LIST1, m_AdapterList);
}


BEGIN_MESSAGE_MAP(ChooseAdapterDialog, CDialog)
END_MESSAGE_MAP()


// ChooseAdapterDialog 消息处理程序
