﻿
// ChildView.cpp : CChildView 类的实现
//

#include "stdafx.h"
#include "MFCApplication1.h"
#include "ChildView.h"
#include "ChooseAdapterDialog.h"
#include "PacketCaptor.h"
#include "network.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CChildView

CChildView::CChildView()
{
    font.CreatePointFont(100, _T("Microsoft YaHei"));
    m_strAdapter = _T("无");
}

CChildView::~CChildView()
{
}


BEGIN_MESSAGE_MAP(CChildView, CWnd)
    ON_WM_PAINT()
    ON_WM_CREATE()
    ON_COMMAND(ID_ChooseAdapter, CChildView::OnChooseadapter)
    ON_BN_CLICKED(IDC_StartButton, CChildView::OnStartButtonClicked)
    ON_BN_CLICKED(IDC_StopButton, CChildView::OnStopButtonClicked)
    ON_WM_SIZE()
    ON_MESSAGE(WM_NEW_PACKET, &CChildView::OnNewPacket)
    ON_MESSAGE(WM_WORKER_THREAD_STOP, &CChildView::OnWorkerThreadStop)
    ON_NOTIFY(LVN_ITEMCHANGED, IDC_PacketsListctrl, OnItemchangedList)
END_MESSAGE_MAP()



// CChildView 消息处理程序


BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs)
{
    if (!CWnd::PreCreateWindow(cs))
        return FALSE;

    cs.dwExStyle |= WS_EX_CLIENTEDGE;
    cs.style &= ~WS_BORDER;
    cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
        ::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);


    return TRUE;
}

void CChildView::OnPaint()
{
    CPaintDC dc(this); // 用于绘制的设备上下文

}

void CChildView::OnChooseadapter()
{
    ChooseAdapterDialog dialog;
    if (dialog.DoModal() == IDOK)
    {
        m_strAdapter = dialog.m_strAdapterName;
        m_staticText.SetWindowText(CString(_T("当前选择的网卡ID为: ")) + m_strAdapter);
        allDevices = dialog.allDevices;
        m_deviceIndex = dialog.m_deviceIndex;

        m_buttonStart.EnableWindow(TRUE);
        //AfxMessageBox(m_strAdapter);
    }
}

int CChildView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CWnd::OnCreate(lpCreateStruct) == -1)
        return -1;


    // 显示当前捕捉的网卡
    CString content(CString(_T("当前选择的网卡ID为: ")) + m_strAdapter);
    m_staticText.Create(content, WS_CHILD | WS_VISIBLE | SS_LEFT, CRect(5, 5, 800, 30), this, IDC_staticText);
    m_staticText.SetFont(&font);

    // 开始和结束捕获的按钮
    m_buttonStart.Create(_T("开始"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, CRect(5, 35, 50, 65), this, IDC_StartButton);
    m_buttonStart.SetFont(&font);
    m_buttonStart.EnableWindow(FALSE);

    m_buttonStop.Create(_T("结束"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, CRect(55, 35, 100, 65), this, IDC_StopButton);
    m_buttonStop.SetFont(&font);
    m_buttonStop.EnableWindow(FALSE);

    m_listctrlPackets.CreateEx(WS_EX_CLIENTEDGE, WS_CHILD | WS_VISIBLE | LVS_REPORT | WS_BORDER | LVS_SINGLESEL, CRect(CPoint(5, 70), CSize(0, 0)), this, IDC_PacketsListctrl);
    m_listctrlPackets.InsertColumn(0, _T("NO."), LVCFMT_LEFT, 80);
    m_listctrlPackets.InsertColumn(1, _T("时间"), LVCFMT_LEFT, 140);
    m_listctrlPackets.InsertColumn(2, _T("源MAC地址"), LVCFMT_LEFT, 160);
    m_listctrlPackets.InsertColumn(3, _T("目的MAC地址"), LVCFMT_LEFT, 160);
    m_listctrlPackets.InsertColumn(4, _T("协议/包长"), LVCFMT_LEFT, 80);
    //m_listctrlPackets.InsertColumn(5, _T("传输层协议"), LVCFMT_LEFT, 90);

    m_treeCtrl.CreateEx(WS_EX_CLIENTEDGE, WS_VISIBLE | WS_TABSTOP | WS_CHILD | WS_BORDER
        | TVS_HASBUTTONS | TVS_LINESATROOT | TVS_HASLINES
        | TVS_DISABLEDRAGDROP | TVS_NOTOOLTIPS | TVS_EDITLABELS, CRect(CPoint(5, 375), CSize(100, 250)), this, IDC_TREEVIEW);
    return 0;
}

void CChildView::addNewPacket(int no, CString time, CString source, CString dest, CString protocol, CString transmit)
{
    LVITEM lvItem;
    int nItem;
    TCHAR buf[1024];


    lvItem.mask = LVIF_TEXT;
    lvItem.iItem = m_listctrlPackets.GetItemCount();
    lvItem.iSubItem = 0;

    _swprintf(buf, _T("%d"), no);
    lvItem.pszText = buf;

    nItem = m_listctrlPackets.InsertItem(&lvItem);

    m_listctrlPackets.SetItemText(nItem, 1, time);

    m_listctrlPackets.SetItemText(nItem, 2, source);
    m_listctrlPackets.SetItemText(nItem, 3, dest);
    m_listctrlPackets.SetItemText(nItem, 4, protocol);
    m_listctrlPackets.SetItemText(nItem, 5, transmit);
    m_listctrlPackets.EnsureVisible(m_listctrlPackets.GetItemCount() - 1, TRUE);

}

void CChildView::showInTreeView(int packetIndex)
{
    m_treeCtrl.DeleteAllItems();
    HTREEITEM hItem, hSubItem;
    TCHAR buf[1024] = { '\0' };
    auto eh = (ethernet_frame_header*)allPackets.GetAt(packetIndex)->pkt_data;

    // Frame
    _swprintf(buf, _T("Frame %d: on interface %d"), packetIndex, m_deviceIndex);
    hItem = m_treeCtrl.InsertItem(buf, TVI_ROOT);
    _swprintf(buf, _T("Interface id: %d (%s)"), m_deviceIndex, m_strAdapter.GetString());
    hSubItem = m_treeCtrl.InsertItem(buf, hItem);
    m_treeCtrl.InsertItem(_T("Encapsulation type: Ethernet (1)"), hItem);
    CTime t = CTime::GetCurrentTime();
    CString strDate = t.Format("%Y.%m.%d");
    _swprintf(buf, _T("Arrival Time: %s %s"), strDate.GetString(), m_listctrlPackets.GetItemText(packetIndex, 1).GetString());
    hSubItem = m_treeCtrl.InsertItem(buf, hItem);

    // Ethernet
    _swprintf(buf, _T("Ethernet II, Src: %s, Dst: %s"), m_listctrlPackets.GetItemText(packetIndex, 2).GetString(),
        m_listctrlPackets.GetItemText(packetIndex, 3).GetString());
    hItem = m_treeCtrl.InsertItem(buf, TVI_ROOT);
    auto header = (ethernet_frame_header*)allPackets.GetAt(packetIndex)->pkt_data;


    if (ntohs(header->type) == 0x0800)
    {
        m_treeCtrl.InsertItem(_T("Type: IPv4 (0x0800)"), hItem);


        auto ih = (ip_header *)((char *)allPackets.GetAt(packetIndex)->pkt_data + sizeof ethernet_frame_header);
        TCHAR protocol[1024];
        /* retireve the position of the udp/tcp header */
        auto ip_len = (ih->ver_ihl & 0xf) * 4;

        int dport, sport;
        switch (ih->proto)
        {
            case 0x06:
            {
                auto th = (tcp_header *)((u_char*)ih + ip_len);
                /* convert from network byte order to host byte order */
                sport = ntohs(th->sport);
                dport = ntohs(th->dport);
                _swprintf(protocol, _T("Protocol: TCP (6)"));
                break;
            }
            case 0x11:
            {
                auto uh = (udp_header *)((u_char*)ih + ip_len);
                /* convert from network byte order to host byte order */
                sport = ntohs(uh->sport);
                dport = ntohs(uh->dport);

                _swprintf(protocol, _T("Protocol: UDP (11)"));
                break;
            }
            default:
                break;
        }

        /* print ip addresses and tcp ports */
        _swprintf(buf, _T("Internet Protocol Version 4, Src: %d.%d.%d.%d:%d, Dst: %d.%d.%d.%d:%d"), ih->saddr.byte1,
            ih->saddr.byte2,
            ih->saddr.byte3,
            ih->saddr.byte4,
            sport,
            ih->daddr.byte1,
            ih->daddr.byte2,
            ih->daddr.byte3,
            ih->daddr.byte4,
            dport);

        hItem = m_treeCtrl.InsertItem(buf, TVI_ROOT);
        _swprintf(buf, _T("Total length: %d"), ntohs(ih->tlen));
        hSubItem = m_treeCtrl.InsertItem(buf, hItem);
        _swprintf(buf, _T("Identification: 0x%x (%d)"), ih->identification, ih->identification);
        hSubItem = m_treeCtrl.InsertItem(buf, hItem);
        _swprintf(buf, _T("Flags: 0x%x"), (ih->flags_fo) >> 13);
        hSubItem = m_treeCtrl.InsertItem(buf, hItem);
        _swprintf(buf, _T("Fragement offset: %d"), ((ih->flags_fo) & 0x1fff));
        hSubItem = m_treeCtrl.InsertItem(buf, hItem);
        _swprintf(buf, _T("Time to live: %d"), ih->ttl);
        hSubItem = m_treeCtrl.InsertItem(buf, hItem);
        hSubItem = m_treeCtrl.InsertItem(protocol, hItem);
        _swprintf(buf, _T("Header checksum: 0x%x"), ih->crc);
        hSubItem = m_treeCtrl.InsertItem(buf, hItem);
        _swprintf(buf, _T("Source: %d.%d.%d.%d"), ih->saddr.byte1, ih->saddr.byte2, ih->saddr.byte3, ih->saddr.byte4);
        hSubItem = m_treeCtrl.InsertItem(buf, hItem);
        _swprintf(buf, _T("Destination: %d:%d:%d:%d"), ih->daddr.byte1, ih->daddr.byte2, ih->daddr.byte3, ih->daddr.byte4);
        hSubItem = m_treeCtrl.InsertItem(buf, hItem);
    }
}

void CChildView::clear()
{
    m_listctrlPackets.DeleteAllItems();
    m_treeCtrl.DeleteAllItems();
    for (int i = 0; i < allPackets.GetSize(); i++)
    {
        delete allPackets.GetAt(i);
    }
    allPackets.RemoveAll();
}

void CChildView::OnStartButtonClicked()
{
    threadInfo = new WorkerThreadInfo();
    threadInfo->allDevices = allDevices;
    threadInfo->index = m_deviceIndex;
    threadInfo->isStop = new int(0);
    threadInfo->uiThreadHandle = m_hWnd;
    AfxBeginThread(PacketCaptor::StartCapture, (LPVOID)threadInfo);
    m_buttonStart.EnableWindow(FALSE);
    m_buttonStop.EnableWindow(TRUE);

    m_packetCount = 0;
    clear();
}

void CChildView::OnStopButtonClicked()
{
    *threadInfo->isStop = 1;
    m_buttonStart.EnableWindow(TRUE);
    m_buttonStop.EnableWindow(FALSE);
}

void CChildView::OnItemchangedList(NMHDR * pNMHDR, LRESULT * pResult)
{
    NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

    if ((pNMListView->uChanged & LVIF_STATE)
        && (pNMListView->uNewState & LVIS_SELECTED))
    {
        POSITION pos = m_listctrlPackets.GetFirstSelectedItemPosition();
        int index = m_listctrlPackets.GetNextSelectedItem(pos);
        showInTreeView(index);
    }
}

void CChildView::OnSize(UINT nType, int cx, int cy)
{
    CWnd::OnSize(nType, cx, cy);
    CRect currentView;
    GetClientRect(&currentView);

    // 设置ListCtrl的宽度
    m_listctrlPackets.SetWindowPos(NULL, 0, 0, currentView.Width() - 5, 300, SWP_NOZORDER | SWP_NOMOVE);
    m_listctrlPackets.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

    // 设置TreeView的宽度
    m_treeCtrl.SetWindowPos(NULL, 0, 0, currentView.Width() - 5, 300, SWP_NOZORDER | SWP_NOMOVE);
}


afx_msg LRESULT CChildView::OnNewPacket(WPARAM wParam, LPARAM lParam)
{
    PacketInfo* packetInfo;
    TCHAR buf[1024];
    packetInfo = (PacketInfo*)wParam;

    allPackets.Add(packetInfo);

    struct tm ltime;
    TCHAR timestr[16];
    time_t local_tv_sec;
    local_tv_sec = packetInfo->header->ts.tv_sec;
    localtime_s(&ltime, &local_tv_sec);
#ifdef _UNICODE
    wcsftime(timestr, sizeof timestr, _T("%H:%M:%S"), &ltime);
#else
    strftime(timestr, sizeof timestr, "%H:%M:%S", &ltime);
#endif
    _swprintf(buf, _T("%s.%.6d"), timestr, packetInfo->header->ts.tv_usec);
    CString time(buf);

    auto ethernetHeader = (ethernet_frame_header*)packetInfo->pkt_data;


    _swprintf(buf, _T("%02X:%02X:%02X:%02X:%02X:%02X"), ethernetHeader->saddr[0], ethernetHeader->saddr[1], ethernetHeader->saddr[2], ethernetHeader->saddr[3], ethernetHeader->saddr[4], ethernetHeader->saddr[5]);
    CString sourceMAC(buf);

    _swprintf(buf, _T("%02X:%02X:%02X:%02X:%02X:%02X"), ethernetHeader->daddr[0], ethernetHeader->daddr[1], ethernetHeader->daddr[2], ethernetHeader->daddr[3], ethernetHeader->daddr[4], ethernetHeader->daddr[5]);

    CString destinationMAC(buf);

    CString protocol;
    CString transmit(_T(""));
    if (ntohs(ethernetHeader->type) <= 1500)
    {
        _swprintf(buf, _T("%hu\n"), ntohs(ethernetHeader->type));
        protocol.SetString(buf);
    }
    else
    {
        switch (ntohs(ethernetHeader->type))
        {
            case 0x0800:
            {
                protocol.SetString(_T("IPv4"));
                auto ih = (ip_header *)((char *)packetInfo->pkt_data + sizeof ethernet_frame_header);
                switch (ih->proto)
                {
                    case 0x06:
                        transmit.SetString(_T("TCP"));
                        break;
                    case 0x11:
                        transmit.SetString(_T("UDP"));
                        break;
                    default:
                        transmit.SetString(_T(""));
                        break;
                }
                break;
            }
            case 0x0806:
                protocol.SetString(_T("ARP"));
                break;
            case 0x8100:
                protocol.SetString(_T("IEEE 802.1Q"));
                break;
            case 0x8137:
                protocol.SetString(_T("IPX"));
                break;
            case 0x86DD:
                protocol.SetString(_T("IPv6"));
                break;
            default:
                protocol.SetString(_T("unknown"));
                break;
        }
    }

    //addNewPacket(m_packetCount++, time, sourceMAC, destinationMAC, protocol, transmit);
    addNewPacket(m_packetCount++, time, sourceMAC, destinationMAC, protocol);
    return 0;
}


afx_msg LRESULT CChildView::OnWorkerThreadStop(WPARAM wParam, LPARAM lParam)
{
    m_buttonStart.EnableWindow(TRUE);
    m_buttonStop.EnableWindow(FALSE);
    return 0;
}
