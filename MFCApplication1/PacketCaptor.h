#pragma once

#include "network.h"
class PacketCaptor
{
public:
    static UINT StartCapture(LPVOID lpParameter);
    ~PacketCaptor();
private:
    PacketCaptor();
};

struct WorkerThreadInfo
{
    HWND uiThreadHandle;
    pcap_if_t* allDevices;
    int index;
    int *isStop;

    static void deleteMyself(WorkerThreadInfo* info);
};

struct PacketInfo
{
    struct pcap_pkthdr *header;
    const u_char* pkt_data;
    PacketInfo()
    {
        header = NULL;
        pkt_data = NULL;
    }

    PacketInfo(pcap_pkthdr* h, const u_char* d)
    {
        header = h;
        pkt_data = d;
    }
};