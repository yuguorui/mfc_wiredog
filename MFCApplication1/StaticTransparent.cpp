// StaticTransparent.cpp : 实现文件
//

#include "stdafx.h"
#include "MFCApplication1.h"
#include "StaticTransparent.h"


// CStaticTransparent

IMPLEMENT_DYNAMIC(CStaticTransparent, CStatic)

CStaticTransparent::CStaticTransparent()
{

}

CStaticTransparent::~CStaticTransparent()
{
}



BEGIN_MESSAGE_MAP(CStaticTransparent, CStatic)
    ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()



// CStaticTransparent 消息处理程序

HBRUSH CStaticTransparent::CtlColor(CDC* pDC, UINT nCtlColor)
{
    pDC->SetTextColor(RGB(0, 0, 0));

    return (HBRUSH)GetStockObject(NULL_BRUSH);
}