#pragma once
#include "stdafx.h"
#include "afxwin.h"

// ChooseAdapterDialog 对话框

class ChooseAdapterDialog : public CDialog
{
	DECLARE_DYNAMIC(ChooseAdapterDialog)

public:
	ChooseAdapterDialog(CWnd* pParent = NULL);   // 标准构造函数

    CString m_strAdapterName;
    pcap_if_t *allDevices = NULL, *tmp = NULL;
    int m_deviceIndex = -1;

    virtual BOOL OnInitDialog() override;
    virtual void OnOK() override;
	virtual ~ChooseAdapterDialog();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ChooseDialog };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持


	DECLARE_MESSAGE_MAP()
public:
    CListBox m_AdapterList;
};
