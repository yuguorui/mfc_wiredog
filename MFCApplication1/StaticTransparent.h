#pragma once


// CStaticTransparent

class CStaticTransparent : public CStatic
{
    DECLARE_DYNAMIC(CStaticTransparent)

public:
    CStaticTransparent();
    virtual ~CStaticTransparent();

protected:
    HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
    DECLARE_MESSAGE_MAP()
};


