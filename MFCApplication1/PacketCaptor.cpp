#include "stdafx.h"
#include "PacketCaptor.h"
#include "network.h"
#include "ChildView.h"


PacketCaptor::PacketCaptor()
{
}


UINT PacketCaptor::StartCapture(LPVOID lpParameter)
{
    TCHAR message[PCAP_BUF_SIZE];

    WorkerThreadInfo* info = (WorkerThreadInfo*)lpParameter;
    pcap_t *adHandle;
    char errbuf[PCAP_ERRBUF_SIZE];
    u_int netmask;

    // Get the name of the target adapter
    pcap_if_t* tmpIf;
    //_swprintf(message, _T("%d"), info->index);
    //AfxMessageBox(message);
    int i = 0;
    for (tmpIf = info->allDevices; i < info->index; i++, tmpIf = tmpIf->next);

    //MultiByteToWideChar(CP_ACP, 0, tmpIf->description, -1, message, PCAP_BUF_SIZE);
    //AfxMessageBox(message);

    /* Open the adapter */
    if ((adHandle = pcap_open(tmpIf->name,  // name of the device
        65536,     // portion of the packet to capture. 
                   // 65536 grants that the whole packet will be captured on all the MACs.
        PCAP_OPENFLAG_PROMISCUOUS,         // promiscuous mode
        1000,      // read timeout
        NULL,      // remote authentication
        errbuf     // error buffer
    )) == NULL)
    {
        TCHAR errbuf_tchar[PCAP_ERRBUF_SIZE];
#ifdef _UNICODE
        MultiByteToWideChar(CP_ACP, 0, errbuf, -1, errbuf_tchar, PCAP_ERRBUF_SIZE);
#endif
        _stprintf(message, _T("\nUnable to open the adapter. %s is not supported by WinPcap\n"), errbuf_tchar);
        AfxMessageBox(message);
        ::PostMessage(info->uiThreadHandle, WM_WORKER_THREAD_STOP, NULL, NULL);
        /* Free the info */
        //pcap_freealldevs(info->allDevices);
        WorkerThreadInfo::deleteMyself(info);
        return -1;
    }


    /* Check the link layer. We support only Ethernet for simplicity. */
    if (pcap_datalink(adHandle) != DLT_EN10MB)
    {
        TCHAR message[100];
        _stprintf(message, _T("\nThis program works only on Ethernet networks.\n"));
        AfxMessageBox(message);
        ::PostMessage(info->uiThreadHandle, WM_WORKER_THREAD_STOP, NULL, NULL);
        /* Free the info */
        //pcap_freealldevs(info->allDevices);
        WorkerThreadInfo::deleteMyself(info);
        return -1;
    }


    if (tmpIf->addresses != NULL)
        /* Retrieve the mask of the first address of the interface */
        netmask = ((struct sockaddr_in *)(tmpIf->addresses->netmask))->sin_addr.S_un.S_addr;
    else
        /* If the interface is without addresses we suppose to be in a C class network */
        netmask = 0xffffff;

    struct bpf_program fcode;
    char packet_filter[] = "";

    //compile the filter
    if (pcap_compile(adHandle, &fcode, packet_filter, 1, netmask) < 0)
    {
        fprintf(stderr, "\nUnable to compile the packet filter. Check the syntax.\n");
        ::PostMessage(info->uiThreadHandle, WM_WORKER_THREAD_STOP, NULL, NULL);
        //pcap_freealldevs(info->allDevices);
        delete info;
        return -1;
    }

    //set the filter
    if (pcap_setfilter(adHandle, &fcode) < 0)
    {
        fprintf(stderr, "\nError setting the filter.\n");
        ::PostMessage(info->uiThreadHandle, WM_WORKER_THREAD_STOP, NULL, NULL);
        //pcap_freealldevs(info->allDevices);
        WorkerThreadInfo::deleteMyself(info);
        return -1;
    }



    int res;
    pcap_pkthdr *header;
    const u_char *pkt_data;
    /* 获取数据包 */
    while ((res = pcap_next_ex(adHandle, &header, &pkt_data)) >= 0 &&
        (*info->isStop) != 1)
    {

        if (res == 0) /* 超时时间到 */
            continue;

        PacketInfo* packetInfo = new PacketInfo(header, pkt_data);

        ::PostMessage(info->uiThreadHandle, WM_NEW_PACKET, (WPARAM)packetInfo, 0);
    }

    if (res == -1)
    {
        TCHAR message[100], errbuf_tchar[PCAP_ERRBUF_SIZE];

#ifdef _UNICODE
        MultiByteToWideChar(CP_ACP, 0, pcap_geterr(adHandle), -1, errbuf_tchar, PCAP_ERRBUF_SIZE);
#endif
        _stprintf(message, _T("Error reading the packets: %s\n"), errbuf_tchar);
        AfxMessageBox(message);

        ::PostMessage(info->uiThreadHandle, WM_WORKER_THREAD_STOP, NULL, NULL);
        // free the info
        //pcap_freealldevs(info->allDevices);
        WorkerThreadInfo::deleteMyself(info);
        return -1;
    }

    // report its status to UI thread.
    ::PostMessage(info->uiThreadHandle, WM_WORKER_THREAD_STOP, NULL, NULL);
    // free the info
    //pcap_freealldevs(info->allDevices);
    WorkerThreadInfo::deleteMyself(info);
    return 0;
}

PacketCaptor::~PacketCaptor()
{
}

void WorkerThreadInfo::deleteMyself(WorkerThreadInfo * info)
{
    delete info->isStop;
    delete info;
}
